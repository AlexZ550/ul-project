INSERT INTO AppUser( Name, Password ) VALUES ('joe', 'b0c8a0f8d6a3f1e4a093ca450b2447b911bf14484694b793330953f0decd8623');           --SHA-256 hash for password 1j?o>E
INSERT INTO AppUser( Name, Password ) VALUES ('administrator', '3eff470f311acd5140a5c327f06eec51b29ac030051e3b73ddcaa2954351fd31'); --SHA-256 hash for password 4u?ido0
INSERT INTO AppUser( Name, Password ) VALUES ('admin', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b');         --SHA-256 hash for password 1
INSERT INTO AppUser( Name, Password ) VALUES ('user', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b');          --SHA-256 hash for password 1

INSERT INTO Role(Name, AppUser) VALUES ( 'Customer', 'user' ); 
INSERT INTO Role(Name, AppUser) VALUES ( 'Administrator', 'admin' ); 
INSERT INTO Role(Name, AppUser) VALUES ( 'Customer', 'joe' ); 
INSERT INTO Role(Name, AppUser) VALUES ( 'Administrator', 'administrator' ); 

INSERT INTO Product( Description, Price, Qty, ImageUrl ) VALUES ( 'Milk', 1, 1000, 'thumbs/1.jpg'); 
INSERT INTO Product(Description, Price, Qty, ImageUrl ) VALUES ( 'Beef', 4, 20, 'thumbs/3.jpg'); 
INSERT INTO Product(Description, Price, Qty, ImageUrl ) VALUES ( 'Vodka', 10, 5, 'thumbs/2.jpg'); 
INSERT INTO Product( Description, Price, Qty, ImageUrl ) VALUES ( 'Apple', 2, 30, 'thumbs/4.jpg'); 




INSERT INTO Log( UserId, Message ) SELECT UserId, 'Test Log Message' FROM AppUser WHERE Name = 'joe'; 
INSERT INTO Log( UserId, Message ) SELECT UserId, 'Test Log Message' FROM AppUser WHERE Name = 'administrator'; 
INSERT INTO Log( UserId, Message ) SELECT UserId, 'Test Log Message' FROM AppUser WHERE Name = 'admin'; 
INSERT INTO Log( UserId, Message ) SELECT UserId, 'Test Log Message' FROM AppUser WHERE Name = 'user'; 


SELECT * FROM Role;
SELECT * FROM AppUser;
SELECT * FROM Product;
SELECT * FROM Log;
