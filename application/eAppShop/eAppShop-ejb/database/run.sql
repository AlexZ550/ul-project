
run './eAppShop-ejb/database/create_database.sql';
run './eAppShop-ejb/database/drop_schema.sql';
run './eAppShop-ejb/database/create_schema.sql';
run './eAppShop-ejb/database/init.sql';

disconnect;