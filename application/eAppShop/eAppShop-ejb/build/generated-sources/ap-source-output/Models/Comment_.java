package Models;

import Models.Appuser;
import Models.Product;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-10-26T21:46:17")
@StaticMetamodel(Comment.class)
public class Comment_ { 

    public static volatile SingularAttribute<Comment, Product> productid;
    public static volatile SingularAttribute<Comment, Integer> commentid;
    public static volatile SingularAttribute<Comment, String> text;
    public static volatile SingularAttribute<Comment, Appuser> userid;
    public static volatile SingularAttribute<Comment, Date> timestamp;

}