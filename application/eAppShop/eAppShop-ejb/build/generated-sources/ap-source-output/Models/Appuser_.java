package Models;

import Models.Comment;
import Models.Log;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-10-26T21:46:17")
@StaticMetamodel(Appuser.class)
public class Appuser_ { 

    public static volatile SingularAttribute<Appuser, String> password;
    public static volatile SingularAttribute<Appuser, Integer> roleid;
    public static volatile CollectionAttribute<Appuser, Comment> commentCollection;
    public static volatile SingularAttribute<Appuser, String> name;
    public static volatile CollectionAttribute<Appuser, Log> logCollection;
    public static volatile SingularAttribute<Appuser, Integer> userid;
    public static volatile SingularAttribute<Appuser, Date> timestamp;

}